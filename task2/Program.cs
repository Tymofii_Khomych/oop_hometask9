﻿namespace task2
{
    internal class Program
    {
        public delegate double MathOperation(double a, double b);

        static void Main(string[] args)
        {
            Console.Write("Enter first number: ");
            double num1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Enter second number: ");
            double num2 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Choose operation '+', '-', '*', '/': ");
            string operation = Console.ReadLine();

            MathOperation Add = (num1, num2) => { return num1 + num2; };
            MathOperation Sub = (num1, num2) => { return num1 - num2; };
            MathOperation Mul = (num1, num2) => { return num1 * num2; };
            MathOperation Div = (num1, num2) => { return num2 == 0 ? 0 : num1 / num2; };

            switch (operation)
            {
                case "+":
                    Console.WriteLine($"\n{num1} + {num2} = {Add(num1, num2)}");
                    break;
                case "-":
                    Console.WriteLine($"\n{num1} - {num2} = {Sub(num1, num2)}");
                    break;
                case "*":
                    Console.WriteLine($"\n{num1} * {num2} = {Mul(num1, num2)}");
                    break;
                case "/":
                    Console.WriteLine($"\n{num1} / {num2} = {Div(num1, num2)}");
                    break;
                default:
                    Console.WriteLine("Unknown operation");
                    break;
            }
        }
    }
}
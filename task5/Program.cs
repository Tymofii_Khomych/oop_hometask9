﻿namespace task5
{
    internal class Program
    {
        public delegate double Average(int a, int b, int c);
        static void Main(string[] args)
        {
            int a = 5, b = 82, c = 43;

            Average avg = (a, b, c) => (double)(a + b + c) / 3;
            Console.WriteLine(avg(a, b, c));
        }
    }
}
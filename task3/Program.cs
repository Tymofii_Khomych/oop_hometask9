﻿namespace task3
{
    internal class Program
    {
        public delegate int GenerateNumber();
        public delegate double GetResult(GenerateNumber[] arr);

        static void Main(string[] args)
        {
            GenerateNumber generate = () => 
            { 
                Random rand = new Random(); 
                return rand.Next(1, 100); 
            };

            var delegate1 = generate;
            var delegate2 = generate;
            var delegate3 = generate;

            GenerateNumber[] array = { delegate1, delegate2, delegate3 };

            GetResult result = (array) =>
            {
                int sum = 0;
                foreach (var del in array)
                    sum += del();
                return (double) sum / array.Length;
            };

            Console.WriteLine(result(array));
        }
    }
}